
<!-- PROJECT LOGO -->
<br />
<p align="center">


  <img src="images/mbd.jpg" alt="Logo" width="80" height="80">

  <h3 align="center">MBD-README-DOCUMENT</h3>

  <p align="center">
    Please read this document before your work with MBD repositories
    <br />
    <strong>MBD team !!!</strong>
    <br />

  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
* [Coding Conventions ](#coding-conventions)
  * [Visual code setting](#visual-code)
  * [Coding style](#coding-style)




<!-- ABOUT THE PROJECT -->
## About The Project

Curently, we have 3 projects for MBD system.
### 1. [MBD Web]()
[![MBD web Name Screen Shot][mbdweb-screenshot]](https://example.com)

This is the main page for run our business. It's include 2 part front-end (for normal user and agent) and admin pannel (for admin)

#### Repository
 [MBD web repository](https://bitbucket.org/mbd-web/mbdweb/src/master/)
#### Built With
* [Bootstrap 3](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Laravel](https://laravel.com)
* [Laravel](https://laravel.com)

### Setup

1. Make sure you have installed [PHP 7](https://www.php.net/downloads.php)
2. Make sure you have installed [Composer](https://getcomposer.org/download/)
3. Import SQL file ( if you don't have SQL file, you can download it [here](https://bitbucket.org/nguyendacanh/db-mbd/src/master/))
4. Run: composer install on your mbd web folder
```sh
composer install
```
5. Create .env file by this command: cp .env.dev .env
```sh
cp .env.dev .env
```
6. Update .env file depend on your enviroment
7. Run it on apache server or serve it

### 2. [MBD backend]()
This is the backend service for MBD system new features.
We will update the api document for it later

#### Repository
 [mbd-admin-back-end](https://bitbucket.org/ywfdn/mbd-admin-back-end/src/master/)
#### Built With
* [Node js -- recommend use version 10.10.0](https://nodejs.org/en/download/)
* [MySQL server -- recommend user version > 8.x (JSON supporting)](hhttps://www.mysql.com/)

### Setup

1. Make sure you have installed [Node js and mySql server]()
3. Import SQL file ( if you don't have SQL file, you can download it [here](https://bitbucket.org/nguyendacanh/db-mbd/src/master/))
4. Run: npm install on your mbd-web-backend folder
```sh
npm install
```
5. Run: npm run start to start the server
```sh
npm run start
```
### 3. [MBD front end admin]()
[![MBD admin Name Screen Shot][mbdadmin-screenshot]](https://example.com)

This is the admin dashboard for MBD system new features.
We will update the api document for it later

#### Repository
 [mbd-admin-front-end-admin](https://bitbucket.org/ywfdn/mbd-admin-front/src/master/)
#### Built With
* [Node js -- recommend use version 10.10.0](https://nodejs.org/en/download/)

### Setup

1. Make sure you have setuped for mbd backend
2. Run: npm install on your mbd-web-front-end-admin folder
```sh
npm install
```
3. Run: npm run start to start the server
```sh
npm run start
```

<!-- GETTING STARTED -->
## Coding Conventions

I will write down the list what we should follow when we work with each other. Reduce the time for PR, improve the quality of code and made code clean are our goal.

### Visual code setting

#### 1. Visual's Extension
  1. Auto close tag ( auto close the HTML tags)
  2. Beautify (Format code)
  3. Blank line organizer (Handle blank line)
  4. Bracket Pair colorizer ( Color for separate bracket)
  5. Empty indent (Hanle empty indent)
  6. ESLint ( Format code and code conversation... don't need to talk more about this)
  7. GitLens (Tracking git history and more ...)
  8. vscode-icons (not require)

  [![visual extensions][vsExtensions]](https://example.com)

#### 2. Visual's config json

Open your code Settings.json and edit like this
```
{
    "workbench.startupEditor": "newUntitledFile",
    "explorer.confirmDelete": false,
    "[html]": {
        "editor.defaultFormatter": "HookyQR.beautify"
    },
    "[javascript]": {
        "editor.defaultFormatter": "HookyQR.beautify"
    },
    "window.titleBarStyle": "custom",
    "javascript.updateImportsOnFileMove.enabled": "always",
    "eslint.alwaysShowStatus": true,
    "files.trimFinalNewlines": true,
    "files.trimTrailingWhitespace": true,
    "breadcrumbs.enabled": true,
    "workbench.editor.tabSizing": "shrink",
    "editor.renderWhitespace": "all",
    "git.autofetch": true,

}
```

#### 3. Config ESLint
1. Add eslintrc.js on your folder (We already implemented it on our repositories so may be we dont need to do it)
This is out rule
```
// http://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    // parser: "babel-eslint",
    // extends: "airbnb-base",
    parserOptions: {
      ecmaVersion: 9,
      sourceType: "module",
      allowImportExportEverywhere: true,
      codeFrame: false
    },
    env: {
      node: true,
      es6: true,
      mocha: true,
    },
    // check if imports actually resolve
    "settings": {
      "import/resolver": {
          "babel-module": {}
      }
    },
    // add your custom rules here
    rules: {
      // enforces spacing around commas
      "comma-spacing": 2,
      // allow unnamed functions
      "func-names": "off",
      // allow requires
      "global-require": "off",
      // no extensions on imports
      "import/extensions": ["off", "never"],
      // imports before anything else
      "import/first": 0,
      // requires new lines after each import
      "import/newline-after-import": "off",
      // wtf
      "import/no-dynamic-require": "off",
      // enforce consistent indentation - use two space for indentation
      "indent": ["error", 2],
      // linebreak style
      "linebreak-style": [
        "error",
        "unix",
      ],
      // enforce a maximum line length
      "max-len": ["error", { "code": 120, "ignoreUrls": true, "ignoreComments": true }],
      // no console (turn back on in production)
      // "no-console": ["error", { allow: ["warn", "error"] }],
      "no-console": "off",
      // allow debugger during development
      "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
      // turn off inability to modify parameters :P
      "no-param-reassign": "off",
      // allow ++/-- assignments
      "no-plusplus": 0,
      // allow return assignments in parentheses
      "no-return-assign": [
        "error",
        "except-parens",
      ],
      // dangling underscores allowed
      "no-underscore-dangle": 0,
      // let functions be hoisted
      "no-use-before-define": [
        "error",
        "nofunc",
      ],
      // disallow empty block statements
      "no-empty": 2,
      // disallow unreachable code after return, throw, continue, and break statements
      "no-unreachable": 2,
      // disallow Unused Variables
      "no-unused-vars": 1,
      // disallow all tabs
      "no-tabs": 2,
      // double quotes only
      "quotes": ["error", "single"],
      // prefer using Error objects as Promise rejection reasons
      "prefer-promise-reject-errors": 2,
      // semi-colons
      "semi": ["error", "always"],
      "arrow-parens": ["error", "as-needed"],
      "arrow-body-style": 0
    }
  };

```

### 3. Coding style
Just my coding style but if you want to update or change something, feel free let me know about it.

<h4 style="color: orange">Naming</h4>

  1. Name of file: the first character will upper case. Ex: UserController.js
  2. Name of class = name of file
  3. Name of function and variable: the first character will lower case. Ex: checkUser()
  4. Name of private function: the first character will be _. ex: _processToken();
  5. Make sure when you named for anything please remember. Short but full meaning. ex: UserController, UserService, getUser ...

<h4 style="color: orange">Thin controller Fat model (Or fat service)</h4>

In the controller or redirect function, we just call the service. Make sure when we read the name of service we will know what exactly this service will do.

<h4 style="color: orange">Each function do one mission</h4>

Yeah, you know what I mean :D

Ex:
```
    login(user) {

        if ( user.name === '') {
            // do something here
        }
        const myUser = User.getUser(....);

        if ( user.password  === md5(myUser)) {
          // do something here
        }

        processToken(user)
        ....
    }
```

I think not good, we will have 2 types for functions. Main functions and private function. => Seperate:
Ex:
```
  login(User) {
    _checkUser(User);
    _getUser(User);
    _comparePassword();
    _processToken();
  }
```

<h4 style="color: orange">Quick return (return early)</h4>

  1. Check the error first
  ex for good case
  ```
    processData(data) {
      if (!data) return null

      // do something else
    }
  ```

<h4 style="color: orange">Defensive programming</h4>

  1. Ex for front-end code
  ```
    <h1> {name || 'Eddy'} </h1>
  ```
  2. Ex for back-end code
  ```
    if(id) {
      // do something
    }
  ```

<h4 style="color: orange">Dependency programming</h4>

 I think I dont need to talk about this any more :D



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=flat-square
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=flat-square
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=flat-square
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=flat-square
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[mbdweb-screenshot]: images/MBDWEB.png
[mbdadmin-screenshot]: images/MBDADMIN.png
[vsExtensions]: images/extensions.png
